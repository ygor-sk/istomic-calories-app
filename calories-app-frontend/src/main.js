// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './components/App'
import router from './router'
import ApiStore from './store/ApiStore'
import LoginStore from './store/LoginStore'
import restClientService from './services/RestClientService'
import Router from "vue-router";

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(Router)

function startApp() {
  new Vue({
    el: '#app',
    router,
    components: {App},
    template: '<App/>'
  })
}

restClientService.axios.get("/api")
  .then(response => {
    ApiStore.data.api = response.data

    // read user logged in from previous session
    LoginStore.methods.init()

    if (LoginStore.methods.isLoggedIn()) {
      restClientService.refreshTokenRequest()
        .then(() =>
          restClientService.securedRequest("get", ApiStore.data.api.accountGetLogged)
            .then(response => {
              LoginStore.data.user = response.data
              startApp()
            })
            .catch(() => {
              LoginStore.methods.clear();
              startApp();
            })
        )
        .catch(() => {
          LoginStore.methods.clear();
          startApp();
        })
    } else {
      startApp();
    }
  })
  .catch(error => {
    console.log(error)
    window.document.getElementById("app").innerText = "Error loading REST API"
  })
