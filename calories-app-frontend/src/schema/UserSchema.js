export default {
  "id": "/User",
  "type": "object",
  "required": true,
  "properties": {
    "id": {
      "type": "number",
    },
    "email": {
      "type": "string",
    },
    "firstName": {
      "type": "string",
    },
    "lastName": {
      "type": "string",
    },
    "dailyCaloriesLimit": {
      "type": "number",
    },
    "role": {
      "type": "string",
    },
  }
}

