import Axios from 'axios';
import uriTemplateService from "uri-template";
import LoginStore from "../store/LoginStore"
import ApiStore from "../store/ApiStore"

const axios = Axios.create({})

const restClientService = {

  /**
   * Calls request with currently stored access token.
   * If 401 is returned then attempts to refresh the token and repeat the request again.
   */
  securedRequest(method, uriTemplate, uriParameters = {}, queryParameters = {}, data = {}) {
    function createDataRequest() {
      return axios.request({
        method,
        url: uriTemplateService.parse(uriTemplate).expand(uriParameters),
        params: queryParameters,
        headers: {
          "Authorization": LoginStore.data.accessToken
        },
        data
      })
    }

    function createDataErrorHandler(reject) {
      return function (dataRequestError) {
        if (dataRequestError.response.status === 403) {
          LoginStore.methods.logoutUser() // do not reject
        } else {
          console.log("REST API error", dataRequestError);
          reject(dataRequestError)
        }
      }
    }

    return new Promise(function (resolve, reject) {
      createDataRequest()
        .then(resolve) // success on first attempt
        .catch(dataRequestError => {
          if (dataRequestError.response.status === 401) { // invalid access token
            restClientService.refreshTokenRequest()
              .then(() => {
                createDataRequest()
                  .then(resolve) // success on second attempt
                  .catch(createDataErrorHandler(reject))
              })
              .catch(() => {
                LoginStore.methods.logoutUser() // invalid refresh token
              })
          } else {
            createDataErrorHandler(reject)(dataRequestError)
          }
        })

    })
  },

  refreshTokenRequest() {
    return new Promise(function (resolve, reject) {
      axios.post(ApiStore.data.api.accountPostRefreshToken, LoginStore.data.refreshToken)
        .then(refreshTokenResponse => {
          LoginStore.data.accessToken = refreshTokenResponse.data.accessToken
          resolve(refreshTokenResponse)
        })
        .catch(reject)
    })
  },

  axios // expose raw axios for custom requests (login, logout)
}

export default restClientService;
