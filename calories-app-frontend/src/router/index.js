// Main

import Router from 'vue-router'
// pages
import LoginPage from '../components/LoginPage'
import RegisterPage from '../components/RegisterPage'
import MealsPage from '../components/MealsPage'
import UsersPage from '../components/UsersPage'
import EntryPage from '../components/EntryPage'
import ContentPage from '../components/ContentPage'
// store
import LoginStore from '../store/LoginStore'

let router = new Router({
    routes: [
      {
        path: '/entry', component: EntryPage
        , children: [
          {path: 'login', component: LoginPage},
          {path: 'register', component: RegisterPage},
        ]
      },
      {
        path: '/content', component: ContentPage
        , children: [
          {path: 'meals', component: MealsPage},
          {path: 'users', component: UsersPage},
        ]
      },
      // otherwise redirect to home
      {path: '*', redirect: '/content/meals'}
    ]
  })
;

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/entry/login', '/entry/register'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = LoginStore.methods.isLoggedIn()

  if (authRequired && !loggedIn) {
    return next({
      path: '/entry/login',
      query: {returnUrl: to.path}
    });
  }

  next();
})

export default router;

