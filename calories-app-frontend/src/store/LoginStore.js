import router from '../router'

const LoginStore = {
  data: {
    refreshToken: null,
    accessToken: null,
    user: null
  },
  methods: {
    clear() {
      LoginStore.data.refreshToken = null
      LoginStore.data.accessToken = null
      LoginStore.data.user = null;
      localStorage.removeItem('refreshToken');
    },
    init() {
      let refreshToken = localStorage.getItem('refreshToken');
      if (refreshToken) {
        LoginStore.data.refreshToken = JSON.parse(refreshToken)
      }
    },
    isLoggedIn() {
      return LoginStore.data.refreshToken !== null;
    },
    loginUser(user, accessToken, refreshToken) {
      LoginStore.data.refreshToken = {
        refreshToken : refreshToken,
        userId : user.id,
      };
      LoginStore.data.accessToken = accessToken;
      LoginStore.data.user = user;
      localStorage.setItem('refreshToken', JSON.stringify(LoginStore.data.refreshToken));
    },
    logoutUser() {
      this.clear();
      router.push("/entry/login");
    }
  }
};

export default LoginStore;
