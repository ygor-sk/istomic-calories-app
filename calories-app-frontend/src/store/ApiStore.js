const ApiStore = {
  data: {
    api: {
      "api": null,
      "accountPutRegister": null,
      "accountPostLogin": null,
      "accountPostLogout": null,
      "accountPostRefreshToken": null,
      "accountGetLogged": null,
      "userGetAll": null,
      "userGetSingle": null,
      "userPostSingleEdit": null,
      "userPostSingleResetPassword": null,
      "mealGetSearch": null,
      "mealGetSearchGrouped": null,
      "mealGetSingle": null,
      "mealPostSingleAdd": null,
      "mealPostSingleEdit": null,
      "mealDeleteSingle": null,
    }
  },
  methods: {}
};

export default ApiStore;
