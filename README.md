#Demo project for Istomic

## User guide

This is a simple app for tracking meals. Users can register, log in and add meals, 
which they have eaten on various days.

Users can than filter meals by name, date, time or group them by days. 
Every meal has calories assigned to them. Users can set a daily limit of calories, 
the app will highlight days, when this setting was exceeded.

User have different roles. A simple USER, a USER_MANAGER and an ADMIN.

## Technical description

Backend is written in Sprint Boot, frontend in Vue.js.

Backend does not have any DB evolutions defined. In order to run the App with 
something different than in-memory H2, the schema needs to be created manually
and DB url needs to be passed using JAR command line properties.

Backend runs on port 8088 by default. Frontend (`npm run dev`) runs on 8080 
and makes requests tp 8088.

Application can be build using Maven `mvn package`. Final JAR contains 
REST API and compiled Vue.js frontend.

Backend can be also build using Gradle. However, the final JAR contains 
only REST API and no frontend.   

## Missing features

* Full Gradle build
* DB migrations (e.g. for MySQL or Postgres)
* Backend is a single module. It should be split at least to WEB module 
  and the rest (services, method security, DB)
  