package sk.ygor.istomic.caloriesapp.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class ResourceForbiddenException extends RuntimeException {

    public ResourceForbiddenException() {
        super(HttpStatus.FORBIDDEN.getReasonPhrase());
    }

    public ResourceForbiddenException(String message) {
        super(message);
    }
}
