package sk.ygor.istomic.caloriesapp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import sk.ygor.istomic.caloriesapp.domain.Meal;
import sk.ygor.istomic.caloriesapp.model.MealSearch;
import sk.ygor.istomic.caloriesapp.model.request.MealEdit;
import sk.ygor.istomic.caloriesapp.model.response.MealSearchResult;
import sk.ygor.istomic.caloriesapp.services.MealService;

import javax.validation.Valid;

@RestController
public class MealController {

    private final MealService mealService;

    public static final String PATH_GET_SEARCH = "/api/user/{userId}/meal/search";
    public static final String PATH_GET_SEARCH_GROUPED = "/api/user/{userId}/meal/searchGrouped";
    public static final String PATH_GET_SINGLE = "/api/user/{userId}/meal/{mealId}";
    public static final String PATH_POST_SINGLE_ADD = "/api/user/{userId}/meal";
    public static final String PATH_POST_SINGLE_EDIT = "/api/user/{userId}/meal/{mealId}";
    public static final String PATH_DELETE_SINGLE = "/api/user/{userId}/meal/{mealId}";

    public MealController(MealService mealService) {
        this.mealService = mealService;
    }

    @RequestMapping(path = PATH_GET_SEARCH, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @Transactional(readOnly = true)
    public @ResponseBody
    MealSearchResult mealGetSearch(@PathVariable Long userId, @Valid MealSearch mealSearch) {
        return mealService.searchMeals(userId, mealSearch);
    }

    @RequestMapping(path = PATH_GET_SEARCH_GROUPED, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @Transactional(readOnly = true)
    public @ResponseBody
    MealSearchResult mealGetSearchGrouped(@PathVariable Long userId, @Valid MealSearch mealSearch) {
        return mealService.searchMealsGrouped(userId, mealSearch);
    }

    @RequestMapping(path = PATH_GET_SINGLE, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @Transactional(readOnly = true)
    public @ResponseBody Meal mealGetSingle(@PathVariable Long userId, @PathVariable Long mealId) {
        return mealService.getMeal(userId, mealId);
    }

    @RequestMapping(path = PATH_POST_SINGLE_EDIT, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    public @ResponseBody Meal mealPostSingleEdit(@PathVariable Long userId,
                                                 @PathVariable Long mealId,
                                                 @Valid @RequestBody MealEdit mealEdit) {
        return mealService.editMeal(userId, mealId, mealEdit);
    }

    @RequestMapping(path = PATH_POST_SINGLE_ADD, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    public @ResponseBody Meal mealPostSingleAdd(@PathVariable Long userId,
                                                @Valid @RequestBody MealEdit mealEdit) {
        return mealService.addMeal(userId, mealEdit);
    }

    @RequestMapping(path = PATH_DELETE_SINGLE, method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    public @ResponseBody Meal mealDeleteSingle(@PathVariable Long userId, @PathVariable Long mealId) {
        return mealService.deleteMeal(userId, mealId);
    }

}
