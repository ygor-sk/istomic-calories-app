package sk.ygor.istomic.caloriesapp.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sk.ygor.istomic.caloriesapp.domain.UserRefreshToken;

import java.util.Optional;

@Repository
public interface UserRefreshTokenRepository extends CrudRepository<UserRefreshToken, Long> {

    @Query("select token from UserRefreshToken token where token.user.id = :userId and token.token = :token")
    Optional<UserRefreshToken> findByUserIdAndToken(@Param("userId") Long userId, @Param("token") String token);

}
