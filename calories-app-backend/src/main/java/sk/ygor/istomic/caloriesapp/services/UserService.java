package sk.ygor.istomic.caloriesapp.services;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sk.ygor.istomic.caloriesapp.domain.User;
import sk.ygor.istomic.caloriesapp.model.exception.ResourceForbiddenException;
import sk.ygor.istomic.caloriesapp.model.exception.WrongPasswordException;
import sk.ygor.istomic.caloriesapp.model.request.ResetPassword;
import sk.ygor.istomic.caloriesapp.model.request.UserEdit;
import sk.ygor.istomic.caloriesapp.repository.UserRepository;
import sk.ygor.istomic.caloriesapp.security.SecurityService;

@Service
public class UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;
    private final PasswordEncoder encoder;
    private final SecurityService securityService;

    @Autowired
    public UserService(UserRepository userRepository,
                       PasswordEncoder encoder, SecurityService securityService) {
        this.userRepository = userRepository;
        this.encoder = encoder;
        this.securityService = securityService;
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER_MANAGER')")
    public Iterable<User> findAllUsers() {
        return userRepository.findAll();
    }

    @PreAuthorize("@securityService.isLoggedUser(#userId) or hasRole('USER_MANAGER') or hasRole('ADMIN')")
    public User findUserById(Long userId) {
        return userRepository.findById(userId)
                .orElseGet(() -> {
                    throw new IllegalStateException();
                });
    }

    @PreAuthorize("(@securityService.isLoggedUser(#userId)) or (hasRole('ADMIN') or hasRole('USER_MANAGER'))")
    public User editUser(Long userId, UserEdit userEdit) {
        if (userEdit.getPasswordChange() != null && !securityService.isLoggedUser(userId)) {
            throw new ResourceForbiddenException("User can only change his own password");
        }
        if (userEdit.getRole() != null && !securityService.getLoggedRole().isAdmin()) {
            throw new ResourceForbiddenException("Only admin can change role");
        }
        return userRepository
                .findById(userId)
                .map(foundUser -> doEditUser(userEdit, foundUser))
                .orElseGet(securityService::handleMissingUser);
    }

    private User doEditUser(UserEdit userEdit, User foundUser) {
        if (userEdit.getPasswordChange() != null) {
            if (encoder.matches(userEdit.getPasswordChange().getCurrentPassword(), foundUser.getPassword())) {
                foundUser.setPassword(encoder.encode(userEdit.getPasswordChange().getNewPassword()));
                LOG.info("Password changed for user: " + foundUser.getId());
            } else {
                throw new WrongPasswordException();
            }
        }

        foundUser.setFirstName(userEdit.getFirstName());
        foundUser.setLastName(userEdit.getLastName());
        foundUser.setDailyCaloriesLimit(userEdit.getDailyCaloriesLimit());
        if (userEdit.getRole() != null) {
            foundUser.setRole(userEdit.getRole());
        }
        userRepository.save(foundUser);
        return foundUser;
    }


    @PreAuthorize("@securityService.isLoggedUser(#userId) or hasRole('ADMIN')")
    public ResetPassword resetPassword(Long userId) {
        return userRepository.findById(userId)
                .map(foundUser -> {
                    String generatedPassword = RandomStringUtils.randomAlphanumeric(10);
                    // TODO: apply same validation
                    foundUser.setPassword(encoder.encode(generatedPassword));
                    userRepository.save(foundUser);
                    return new ResetPassword(generatedPassword);
                })
                .orElseGet(securityService::handleMissingUser);
    }
}
