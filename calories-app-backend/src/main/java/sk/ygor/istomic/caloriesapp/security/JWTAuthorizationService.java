package sk.ygor.istomic.caloriesapp.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
import sk.ygor.istomic.caloriesapp.domain.User;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@Component
public class JWTAuthorizationService {

    private static final Logger LOG = LoggerFactory.getLogger(JWTAuthorizationService.class);

    private static final String HEADER_STRING = "Authorization";
    private static final String TOKEN_PREFIX = "Bearer ";
    private static final String CLAIM_ROLE = "Role";

    private final String jwtSecret;
    private final Integer jwtExpirationSeconds;

    public JWTAuthorizationService(@Value("${jwt.secret}") String jwtSecret,
                                   @Value("${jwt.expiration.seconds}") Integer jwtExpirationSeconds) {
        this.jwtSecret = jwtSecret;
        this.jwtExpirationSeconds = jwtExpirationSeconds;
    }

    /**
     * We have made this filter responsible for creating access tokens too.
     * This way, we keep all functionality regarding JWTs in a single place.
     */
    public String generateAccessToken(long userId, User.Role role) {
        return TOKEN_PREFIX + JWT.create()
                .withSubject(String.valueOf(userId))
                .withClaim(CLAIM_ROLE, role.name())
                .withIssuedAt(new Date())
                .withExpiresAt(new Date(System.currentTimeMillis() + jwtExpirationSeconds * 1000))
                .sign(Algorithm.HMAC256(jwtSecret.getBytes()));
    }

    /*
     * This filter uses injected dependencies, but we do not want to register it as a bean,
     * because Spring will register it as a bean too. This would lead to having the filter executed twice.
     *
     * https://stackoverflow.com/questions/39314176/filter-invoke-twice-when-register-as-spring-bean
     */
    public GenericFilterBean createFilter() {
        return new GenericFilterBean() {
            @Override public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
                // our filter only works with HTTP
                HttpServletRequest httpRequest = (HttpServletRequest) request;
                HttpServletResponse httpResponse = (HttpServletResponse) response;

                String token = httpRequest.getHeader(HEADER_STRING);
                if (token != null) {
                    SecurityContextHolder.getContext().setAuthentication(getAuthentication(token));
                }
                chain.doFilter(httpRequest, httpResponse);
            }
        };
    }

    private Authentication getAuthentication(String token) {
        // parse the token
        final DecodedJWT jwt;
        try {
            jwt = JWT.require(Algorithm.HMAC256(jwtSecret.getBytes()))
                    .build()
                    .verify(token.replace(TOKEN_PREFIX, ""));
        } catch (JWTVerificationException e) {
            LOG.debug("Invalid JWT", e);
            return null;
        }

        final long userId;
        try {
            userId = Long.parseLong(jwt.getSubject());
        } catch (NumberFormatException e) {
            LOG.debug("Invalid JWT. Subject is not an user ID");
            return null;
        }

        // this behaves like a pure resource server, which does not have access to user's table and roles
        // in this demo app, resource and security server are the same - we could read user's current role though easily here

        final User.Role role;
        try {
            role = User.Role.valueOf(jwt.getClaim(CLAIM_ROLE).asString());
        } catch (IllegalArgumentException e) {
            LOG.debug("Invalid JWT. Role is not a valid user role");
            return null;
        }

        LOG.debug("Valid JWT. User ID: " + userId);

        return new JWTAuthentication(userId, role);
    }

}
