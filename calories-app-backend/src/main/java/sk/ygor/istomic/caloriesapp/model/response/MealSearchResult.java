package sk.ygor.istomic.caloriesapp.model.response;

import org.springframework.data.domain.Page;

import java.util.Map;

public class MealSearchResult<ID, DATA> {

    public enum CaloriesStatus {
        BELOW_LIMIT,
        EXCEEDED,
    }

    private final long totalCalories;
    private final Page<DATA> page;
    private final Map<ID, CaloriesStatus> caloriesStatus;

    public MealSearchResult(long totalCalories, Page<DATA> page, Map<ID, CaloriesStatus> caloriesStatus) {
        this.totalCalories = totalCalories;
        this.page = page;
        this.caloriesStatus = caloriesStatus;
    }

    public long getTotalCalories() {
        return totalCalories;
    }

    public Page<DATA> getPage() {
        return page;
    }

    public Map<ID, CaloriesStatus> getCaloriesStatus() {
        return caloriesStatus;
    }

    @Override public String toString() {
        return "MealSearchResult{" +
                "totalCalories=" + totalCalories +
                ", page=" + page +
                ", caloriesStatus=" + caloriesStatus +
                '}';
    }

}
