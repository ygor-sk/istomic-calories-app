package sk.ygor.istomic.caloriesapp.model.response;

import sk.ygor.istomic.caloriesapp.domain.User;

public class UserLoginResponse {

    private final User user;
    private final String accessToken;
    private final String refreshToken;

    public UserLoginResponse(User user, String accessToken, String refreshToken) {
        this.user = user;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public User getUser() {
        return user;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }
}
