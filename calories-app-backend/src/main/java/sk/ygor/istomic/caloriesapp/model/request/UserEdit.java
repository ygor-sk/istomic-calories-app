package sk.ygor.istomic.caloriesapp.model.request;

import sk.ygor.istomic.caloriesapp.domain.User;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

public class UserEdit {

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @Digits(integer = 7, fraction = 0)
    @PositiveOrZero
    private int dailyCaloriesLimit;

    @Valid
    private User.Role role;

    @Valid
    private PasswordChange passwordChange;

    public UserEdit() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public PasswordChange getPasswordChange() {
        return passwordChange;
    }

    public void setPasswordChange(PasswordChange passwordChange) {
        this.passwordChange = passwordChange;
    }

    public int getDailyCaloriesLimit() {
        return dailyCaloriesLimit;
    }

    public void setDailyCaloriesLimit(int dailyCaloriesLimit) {
        this.dailyCaloriesLimit = dailyCaloriesLimit;
    }

    public User.Role getRole() {
        return role;
    }

    public void setRole(User.Role role) {
        this.role = role;
    }
}
