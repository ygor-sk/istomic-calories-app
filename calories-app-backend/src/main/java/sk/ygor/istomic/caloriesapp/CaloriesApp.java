package sk.ygor.istomic.caloriesapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

@SpringBootApplication
public class CaloriesApp {

    public static final String PROFILE_LOAD_DUMMY_DATA = "loadDummyData";
    public static final String PROFILE_LOAD_DEFAULT_ADMIN = "loadDefaultAdmin";

    public static void main(String[] args) {
        SpringApplication.run(CaloriesApp.class, args);
    }

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.ENGLISH);
        return slr;
    }

}
