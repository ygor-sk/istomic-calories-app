package sk.ygor.istomic.caloriesapp.model.response;

import sk.ygor.istomic.caloriesapp.domain.Meal;
import sk.ygor.istomic.caloriesapp.domain.User;

import java.util.Optional;

@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
public class UserMeal {

    private final User user;
    private final Optional<Meal> mealOptional;

    public UserMeal(User user, Optional<Meal> mealOptional) {
        this.user = user;
        this.mealOptional = mealOptional;
    }

    public User getUser() {
        return user;
    }

    public Optional<Meal> getMealOptional() {
        return mealOptional;
    }
}
