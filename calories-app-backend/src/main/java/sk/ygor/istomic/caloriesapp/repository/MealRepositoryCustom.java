package sk.ygor.istomic.caloriesapp.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;
import sk.ygor.istomic.caloriesapp.domain.MealGrouped;
import sk.ygor.istomic.caloriesapp.domain.Meal;
import sk.ygor.istomic.caloriesapp.model.MealSearch;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository
public class MealRepositoryCustom {

    private final EntityManager em;

    @Autowired
    public MealRepositoryCustom(EntityManager em) {
        this.em = em;
    }

    public long calculateTotalCalories(Specification<Meal> specification) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Integer> query = criteriaBuilder.createQuery(Integer.class);
        Root<Meal> root = createMealsRoot(specification, criteriaBuilder, query);
        query.select(criteriaBuilder.sum(root.get("calories")));
        TypedQuery<Integer> typedQuery = em.createQuery(query);

        return Optional.ofNullable(typedQuery.getSingleResult()).orElse(0);
    }

    public Page<MealGrouped> findAllGrouped(Specification<Meal> specification,
                                            MealSearch.SortColumn sortColumn, Sort.Direction sortDirection,
                                            int pageNumber, int pageSize) {
        return new PageImpl<>(
                getGroupedByDay(specification, sortColumn, sortDirection, pageNumber, pageSize),
                PageRequest.of(pageNumber, pageSize),
                getGroupedTotal(specification)
        );
    }

    private List<MealGrouped> getGroupedByDay(Specification<Meal> specification,
                                              MealSearch.SortColumn sortColumn,
                                              Sort.Direction sortDirection,
                                              int pageNumber, int pageSize) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<MealGrouped> query = criteriaBuilder.createQuery(MealGrouped.class);
        Root<Meal> root = query.from(Meal.class);
        query.where(specification.toPredicate(root, query, criteriaBuilder));
        query.groupBy(root.get("localDate"));

        Expression<? extends Number> sortBy = null;

        if (sortColumn.equals(MealSearch.SortColumn.dateTime)) {
            sortBy = root.get("localDate");
        }
        if (sortColumn.equals(MealSearch.SortColumn.calories)) {
            sortBy = criteriaBuilder.sum(root.get("calories"));
        }
        if (sortColumn.equals(MealSearch.SortColumn.description)) {
            throw new UnsupportedOperationException("cannot sort by description when grouped");
        }
        if (sortColumn.equals(MealSearch.SortColumn.count)) {
            sortBy = criteriaBuilder.count(root.get("calories"));
        }
        if (sortBy != null) {
            query.orderBy(sortDirection == Sort.Direction.ASC ? criteriaBuilder.asc(sortBy) : criteriaBuilder.desc(sortBy));
        }

        query.select(criteriaBuilder.construct(MealGrouped.class,
                root.get("localDate"),
                criteriaBuilder.count(root.get("calories")),
                criteriaBuilder.sum(root.get("calories"))
        ));

        TypedQuery<MealGrouped> typedQuery = em.createQuery(query);
        typedQuery.setFirstResult(pageNumber * pageSize);
        typedQuery.setMaxResults(pageSize);
        return typedQuery.getResultList();
    }

    private long getGroupedTotal(Specification<Meal> specification) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> query = criteriaBuilder.createQuery(Long.class);
        Root<Meal> root = createMealsRoot(specification, criteriaBuilder, query);

        query.select(criteriaBuilder.countDistinct(root.get("localDate")));
        // https://stackoverflow.com/questions/5423937/how-do-i-count-the-number-of-rows-returned-by-subquery
        return Optional.ofNullable(em.createQuery(query).getSingleResult()).orElse(0L);
    }

    private <T extends Number> Root<Meal> createMealsRoot(Specification<Meal> specification, CriteriaBuilder criteriaBuilder, CriteriaQuery<T> query) {
        Root<Meal> root = query.from(Meal.class);
        query.where(specification.toPredicate(root, query, criteriaBuilder));
        return root;
    }

}
