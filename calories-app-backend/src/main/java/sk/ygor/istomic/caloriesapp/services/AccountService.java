package sk.ygor.istomic.caloriesapp.services;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sk.ygor.istomic.caloriesapp.domain.User;
import sk.ygor.istomic.caloriesapp.domain.UserRefreshToken;
import sk.ygor.istomic.caloriesapp.model.exception.DuplicateEmailEception;
import sk.ygor.istomic.caloriesapp.model.exception.RefreshTokenInvalidException;
import sk.ygor.istomic.caloriesapp.model.exception.UserNotFoundException;
import sk.ygor.istomic.caloriesapp.model.exception.WrongPasswordException;
import sk.ygor.istomic.caloriesapp.model.request.UserLogin;
import sk.ygor.istomic.caloriesapp.model.request.UserRegistration;
import sk.ygor.istomic.caloriesapp.model.response.AccessToken;
import sk.ygor.istomic.caloriesapp.model.response.UserLoginResponse;
import sk.ygor.istomic.caloriesapp.repository.UserRefreshTokenRepository;
import sk.ygor.istomic.caloriesapp.repository.UserRepository;
import sk.ygor.istomic.caloriesapp.security.JWTAuthorizationService;

import java.util.Optional;

@Service
public class AccountService {

    private static final Logger LOG = LoggerFactory.getLogger(AccountService.class);

    private final UserRepository userRepository;
    private final UserRefreshTokenRepository userRefreshTokenRepository;
    private final PasswordEncoder encoder;
    private final JWTAuthorizationService jwtAuthorizationService;

    @Autowired
    public AccountService(UserRepository userRepository,
                          UserRefreshTokenRepository userRefreshTokenRepository,
                          PasswordEncoder encoder,
                          JWTAuthorizationService jwtAuthorizationService) {
        this.userRepository = userRepository;
        this.userRefreshTokenRepository = userRefreshTokenRepository;
        this.encoder = encoder;
        this.jwtAuthorizationService = jwtAuthorizationService;
    }

    public User registerUser(UserRegistration userRegistration) {
        Optional<User> existingUser = userRepository.findByEmail(userRegistration.getEmail());
        if (existingUser.isPresent()) {
            throw new DuplicateEmailEception();
        }

        User user = new User(
                userRegistration.getEmail(),
                userRegistration.getFirstName(),
                userRegistration.getLastName(),
                encoder.encode(userRegistration.getPassword()),
                User.Role.USER,
                0 // TODO: make part of registration
        );
        userRepository.save(user);
        LOG.info(user.toString() + " successfully saved into DB");
        return user;

    }

    public UserLoginResponse loginUser(UserLogin userLogin) {
        return userRepository.findByEmail(userLogin.getEmail())
                .map(user -> {
                    if (encoder.matches(userLogin.getPassword(), user.getPassword())) {
                        LOG.info("User logged in: " + userLogin.getEmail());
                        String accessToken = jwtAuthorizationService.generateAccessToken(user.getId(), user.getRole());
                        String refreshToken = createRefreshToken(user);
                        return new UserLoginResponse(user, accessToken, refreshToken);
                    } else {
                        LOG.info("Wrong password for user : " + userLogin.getEmail());
                        throw new WrongPasswordException();
                    }
                })
                .orElseGet(() -> {
                            LOG.info("User not found: " + userLogin.getEmail());
                            throw new UserNotFoundException();
                        }
                );
    }

    private String createRefreshToken(User user) {
        String token = RandomStringUtils.randomAlphanumeric(128);
        userRefreshTokenRepository.save(new UserRefreshToken(token, user));
        return token;
    }

    public AccessToken refreshAccessToken(long userId, String refreshToken) {
        return userRefreshTokenRepository.findByUserIdAndToken(userId, refreshToken)
                .map(userRefreshToken -> new AccessToken(
                        jwtAuthorizationService.generateAccessToken(
                                userRefreshToken.getUser().getId(),
                                userRefreshToken.getUser().getRole()
                        )
                ))
                .orElseThrow(RefreshTokenInvalidException::new);
    }

    public void logoutUser(long userId, String refreshToken) {
        userRefreshTokenRepository.findByUserIdAndToken(userId, refreshToken)
                .ifPresent(userRefreshTokenRepository::delete);
    }

}
