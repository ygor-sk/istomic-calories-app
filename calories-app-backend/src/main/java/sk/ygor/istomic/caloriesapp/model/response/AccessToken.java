package sk.ygor.istomic.caloriesapp.model.response;

public class AccessToken {

    private final String accessToken;

    public AccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }
}
