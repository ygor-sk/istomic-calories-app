package sk.ygor.istomic.caloriesapp.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import sk.ygor.istomic.caloriesapp.CaloriesApp;
import sk.ygor.istomic.caloriesapp.domain.Meal;
import sk.ygor.istomic.caloriesapp.domain.User;
import sk.ygor.istomic.caloriesapp.repository.MealRepository;
import sk.ygor.istomic.caloriesapp.repository.UserRepository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.IntStream;

@Component()
public class DatabaseLoader implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(DatabaseLoader.class);

    private final UserRepository userRepository;
    private final MealRepository mealRepository;
    private final PasswordEncoder encoder;
    private final Environment environment;

    @Autowired
    public DatabaseLoader(UserRepository userRepository,
                          MealRepository mealRepository,
                          PasswordEncoder encoder,
                          Environment environment) {
        this.userRepository = userRepository;
        this.mealRepository = mealRepository;
        this.encoder = encoder;
        this.environment = environment;
    }

    private List<User> generateUsers() {
        return Arrays.asList(
                new User("admin@calories-tracker.com", "David", "Stern", "admin", User.Role.ADMIN, 2000),
                new User("manager@calories-tracker.com", "Phil", "Jackson", "manager", User.Role.USER_MANAGER, 2000),
                new User("mj@calories-tracker.com", "Michael", "Jordan", "mj", User.Role.USER, 2000),
                new User("lj@calories-tracker.com", "LeBron", "James", "lj", User.Role.USER, 2000),
                new User("td@calories-tracker.com", "Tim", "Duncan", "td", User.Role.USER, 2000)
        );
    }

    private List<Meal> generateMeals(User user) {
        Random random = new Random(user.getId());
        List<Meal> result = new ArrayList<>();
        IntStream.range(0, random.nextInt(100) + 50).forEach(dayIndex -> {
            LocalDate day = LocalDate.of(2018, Month.OCTOBER, 1).plus(dayIndex, ChronoUnit.DAYS);
            IntStream.range(0, random.nextInt(4)).forEach(mealIndex ->
                    result.add(generateMeal(user, random, day, mealIndex, 6, "breakfast"))
            );
            IntStream.range(0, random.nextInt(4)).forEach(mealIndex ->
                    result.add(generateMeal(user, random, day, mealIndex, 11, "lunch"))
            );
            IntStream.range(0, random.nextInt(4)).forEach(mealIndex ->
                    result.add(generateMeal(user, random, day, mealIndex, 18, "supper"))
            );
        });
        return result;

    }

    private Meal generateMeal(User user, Random random, LocalDate day, int mealIndex, int startingHour, final String dayPart) {
        return new Meal(
                day,
                LocalTime.of(mealIndex + startingHour, random.nextInt(60)),
                generateMealName(random) + " for " + dayPart,
                (random.nextInt(500) + 50) * 10,
                user
        );
    }

    private String generateMealName(Random random) {
        String[] mainDish = new String[]{"Chicken", "Pork", "Veal", "Beef"};
        String[] sideDish = new String[]{"Rice", "Bread", "Potatoes", "French fries", "Salad", "Spaghetti"};
        String[] drink = new String[]{"Water", "Cola", "Beer", "Milk"};
        return String.format("%s with %s and %s",
                mainDish[random.nextInt(4)],
                sideDish[random.nextInt(6)],
                drink[random.nextInt(4)]
        );
    }

    @Override
    public void run(String... args) {
        List<String> activeProfiles = Arrays.asList(this.environment.getActiveProfiles());
        if (activeProfiles.contains(CaloriesApp.PROFILE_LOAD_DUMMY_DATA)) {
            loadDummyData();
        }
        if (activeProfiles.contains(CaloriesApp.PROFILE_LOAD_DEFAULT_ADMIN)) {
            loadDefaultAdmin();
        }
    }

    private void loadDummyData() {
        LOG.info("Creating dummy data");
        List<User> users = generateUsers();
        users.forEach(user -> {
            user.setPassword(encoder.encode(user.getPassword()));
            this.userRepository.save(user);
        });
        users.forEach(user ->
                generateMeals(user).forEach(meal -> {
                    meal.setUser(user);
                    mealRepository.save(meal);
                })
        );
    }

    private void loadDefaultAdmin() {
        Optional<User> existingAdmin = this.userRepository.findByEmail("igor.inas@gmail.com");
        if (existingAdmin.isPresent()) {
            LOG.info("Default admin already exists");
        } else {
            LOG.info("Creating default admin");
            this.userRepository.save(
                    new User("igor.inas@gmail.com", "Igor", "Inas", encoder.encode("calories"), User.Role.ADMIN, 0)
            );
        }
    }
}
