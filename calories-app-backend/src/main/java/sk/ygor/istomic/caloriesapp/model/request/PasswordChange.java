package sk.ygor.istomic.caloriesapp.model.request;

import sk.ygor.istomic.caloriesapp.model.validation.ValidPassword;

import javax.validation.constraints.NotBlank;

public class PasswordChange {

    @NotBlank
    private String currentPassword;

    @ValidPassword
    private String newPassword;

    public PasswordChange() {
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
