package sk.ygor.istomic.caloriesapp.controller;

import com.google.common.collect.ImmutableMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class ApiController {

    public static final String PATH_API = "/api";

    @RequestMapping(path = PATH_API, method = RequestMethod.GET)
    public @ResponseBody
    Map<String, String> apiLinks() {
        return ImmutableMap.<String, String>builder()
                .put("api", PATH_API)
                .put("accountPutRegister", AccountController.PATH_REGISTER)
                .put("accountPostLogin", AccountController.PATH_LOGIN)
                .put("accountPostLogout", AccountController.PATH_LOGOUT)
                .put("accountPostRefreshToken", AccountController.PATH_REFRESH_TOKEN)
                .put("accountGetLogged", AccountController.PATH_GET_LOGGED)
                .put("userGetAll", UserController.PATH_GET_ALL)
                .put("userGetSingle", UserController.PATH_GET_SINGLE)
                .put("userPostSingleEdit", UserController.PATH_POST_SINGLE_EDIT)
                .put("userPostSingleResetPassword", UserController.PATH_POST_SINGLE_RESET_PASSWORD)
                .put("mealGetSearch", MealController.PATH_GET_SEARCH)
                .put("mealGetSearchGrouped", MealController.PATH_GET_SEARCH_GROUPED)
                .put("mealGetSingle", MealController.PATH_GET_SINGLE)
                .put("mealPostSingleAdd", MealController.PATH_POST_SINGLE_ADD)
                .put("mealPostSingleEdit", MealController.PATH_POST_SINGLE_EDIT)
                .put("mealDeleteSingle", MealController.PATH_DELETE_SINGLE)
                .build();
    }

}
