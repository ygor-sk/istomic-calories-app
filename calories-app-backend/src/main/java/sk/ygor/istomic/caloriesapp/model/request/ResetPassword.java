package sk.ygor.istomic.caloriesapp.model.request;

public class ResetPassword {

    private String newPassword;

    public ResetPassword() {
    }

    public ResetPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
