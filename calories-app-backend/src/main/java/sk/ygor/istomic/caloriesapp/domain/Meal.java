package sk.ygor.istomic.caloriesapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
public class Meal {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private LocalDate localDate;

    @Column(nullable = false)
    private LocalTime localTime;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private int calories;

    @ManyToOne
    @JsonIgnore
    private User user;

    protected Meal() {
    }

    public Meal(LocalDate localDate, LocalTime localTime, String description, int calories, User user) {
        this.localDate = localDate;
        this.localTime = localTime;
        this.description = description;
        this.calories = calories;
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public LocalTime getLocalTime() {
        return localTime;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public void setLocalTime(LocalTime localTime) {
        this.localTime = localTime;
    }

    public Meal withId(Long id) {
        this.id = id;
        return this;
    }

    @Override public String toString() {
        return "Meal{" +
                "id=" + id +
                ", localDate=" + localDate +
                ", localTime=" + localTime +
                ", description='" + description + '\'' +
                ", calories=" + calories +
                ", user=" + user +
                '}';
    }

}
