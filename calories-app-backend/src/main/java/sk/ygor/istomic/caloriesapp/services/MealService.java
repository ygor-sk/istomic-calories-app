package sk.ygor.istomic.caloriesapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import sk.ygor.istomic.caloriesapp.domain.Meal;
import sk.ygor.istomic.caloriesapp.domain.MealGrouped;
import sk.ygor.istomic.caloriesapp.domain.User;
import sk.ygor.istomic.caloriesapp.model.MealSearch;
import sk.ygor.istomic.caloriesapp.model.exception.MealNotFoundException;
import sk.ygor.istomic.caloriesapp.model.exception.ResourceForbiddenException;
import sk.ygor.istomic.caloriesapp.model.request.MealEdit;
import sk.ygor.istomic.caloriesapp.model.response.MealSearchResult;
import sk.ygor.istomic.caloriesapp.model.response.UserMeal;
import sk.ygor.istomic.caloriesapp.repository.MealRepository;
import sk.ygor.istomic.caloriesapp.repository.MealRepositoryCustom;
import sk.ygor.istomic.caloriesapp.repository.UserRepository;
import sk.ygor.istomic.caloriesapp.security.SecurityService;

import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class MealService {

    private final UserRepository userRepository;
    private final MealRepository mealRepository;
    private final MealRepositoryCustom mealRepositoryCustom;
    private final SecurityService securityService;

    @Autowired
    public MealService(UserRepository userRepository,
                       MealRepository mealRepository,
                       MealRepositoryCustom mealRepositoryCustom,
                       SecurityService securityService) {
        this.userRepository = userRepository;
        this.mealRepository = mealRepository;
        this.mealRepositoryCustom = mealRepositoryCustom;
        this.securityService = securityService;
    }

    @PreAuthorize("@securityService.isLoggedUser(#userId) or hasRole('ADMIN')")
    public MealSearchResult<Long, Meal> searchMeals(Long userId, MealSearch search) {
        return userRepository.findById(userId)
                .map(user -> doSearchMeals(user, withDefaults(search)))
                .orElseGet(securityService::handleMissingUser);
    }

    @PreAuthorize("@securityService.isLoggedUser(#userId) or hasRole('ADMIN')")
    public MealSearchResult<LocalDate, MealGrouped> searchMealsGrouped(Long userId, MealSearch search) {
        return userRepository.findById(userId)
                .map(user -> doSearchMealsGrouped(user, withDefaults(search)))
                .orElseGet(securityService::handleMissingUser);
    }

    private MealSearchResult<Long, Meal> doSearchMeals(User user, MealSearch search) {
        Specification<Meal> specification = createSpecification(user.getId(), search);
        Sort sort = createSort(search);
        PageRequest pageRequest = PageRequest.of(search.getPageNumber(), search.getPageSize(), sort);

        Page<Meal> page = mealRepository.findAll(specification, pageRequest);
        return new MealSearchResult<>(
                mealRepositoryCustom.calculateTotalCalories(specification),
                page,
                calculateDailyLimitStatusMap(user, page.getContent())
        );
    }

    private Sort createSort(MealSearch search) {
        switch (search.getSortColumn()) {
            case dateTime:
                return Sort.by(search.getSortDirection(), "localDate", "localTime");
            case description:
                return Sort.by(search.getSortDirection(), "description");
            case calories:
                return Sort.by(search.getSortDirection(), "calories");
            default:
                throw new UnsupportedOperationException(search.getSortColumn().name());
        }
    }

    private MealSearchResult<LocalDate, MealGrouped> doSearchMealsGrouped(User user, MealSearch search) {
        Specification<Meal> specification = createSpecification(user.getId(), search);
        Page<MealGrouped> allGrouped = mealRepositoryCustom.findAllGrouped(
                specification, search.getSortColumn(), search.getSortDirection(),
                search.getPageNumber(), search.getPageSize()
        );
        return new MealSearchResult<>(
                mealRepositoryCustom.calculateTotalCalories(specification),
                allGrouped,
                getLocalDateCaloriesStatusMap(user, allGrouped.getContent(), MealGrouped::getLocalDate)
        );
    }

    private Map<Long, MealSearchResult.CaloriesStatus> calculateDailyLimitStatusMap(User user, List<Meal> pageMeals) {
        Map<LocalDate, MealSearchResult.CaloriesStatus> dayCalories = getLocalDateCaloriesStatusMap(user, pageMeals, Meal::getLocalDate);

        Map<Long, MealSearchResult.CaloriesStatus> result = new LinkedHashMap<>();
        pageMeals.forEach(meal -> result.put(meal.getId(), dayCalories.get(meal.getLocalDate())));
        return result;
    }

    private <T> Map<LocalDate, MealSearchResult.CaloriesStatus> getLocalDateCaloriesStatusMap(User user, List<T> pageMeals, Function<T, LocalDate> extractLocalDate) {
        Set<LocalDate> days = pageMeals.stream().map(extractLocalDate).collect(Collectors.toSet());
        Map<LocalDate, MealSearchResult.CaloriesStatus> dayCalories = new LinkedHashMap<>();
        days.forEach(day -> {
            int calories = mealRepository.getSumOfCalories(user.getId(), day);
            MealSearchResult.CaloriesStatus caloriesStatus = calories > user.getDailyCaloriesLimit() ?
                    MealSearchResult.CaloriesStatus.EXCEEDED :
                    MealSearchResult.CaloriesStatus.BELOW_LIMIT;
            dayCalories.put(day, caloriesStatus);
        });
        return dayCalories;
    }

    private Specification<Meal> createSpecification(Long userId, MealSearch mealSearch) {
        return Specification.where(
                (root, query, criteriaBuilder) -> criteriaBuilder.and(
                        Stream.of(
                                criteriaBuilder.equal(root.get("user").get("id"), userId),
                                mealSearch.getDescriptionLike() == null ? null : criteriaBuilder.like(criteriaBuilder.lower(root.get("description")), "%" + mealSearch.getDescriptionLike().toLowerCase() + "%"),
                                mealSearch.getDateFrom() == null ? null : criteriaBuilder.greaterThanOrEqualTo(root.get("localDate"), mealSearch.getDateFrom()),
                                mealSearch.getDateTo() == null ? null : criteriaBuilder.lessThanOrEqualTo(root.get("localDate"), mealSearch.getDateTo()),
                                mealSearch.getTimeFrom() == null ? null : criteriaBuilder.greaterThanOrEqualTo(root.get("localTime"), mealSearch.getTimeFrom()),
                                mealSearch.getTimeTo() == null ? null : criteriaBuilder.lessThanOrEqualTo(root.get("localTime"), mealSearch.getTimeTo()),
                                mealSearch.getCaloriesFrom() == null ? null : criteriaBuilder.greaterThanOrEqualTo(root.get("calories"), mealSearch.getCaloriesFrom()),
                                mealSearch.getCaloriesTo() == null ? null : criteriaBuilder.lessThanOrEqualTo(root.get("calories"), mealSearch.getCaloriesTo())
                        ).filter(Objects::nonNull).toArray(Predicate[]::new)
                )
        );
    }

    private MealSearch withDefaults(MealSearch mealSearch) {
        return new MealSearch(
                mealSearch.getDescriptionLike(),
                mealSearch.getDateFrom(),
                mealSearch.getDateTo(),
                mealSearch.getTimeFrom(),
                mealSearch.getTimeTo(),
                mealSearch.getCaloriesFrom(),
                mealSearch.getCaloriesTo(),
                Optional.ofNullable(mealSearch.getPageNumber()).orElse(0),
                Optional.ofNullable(mealSearch.getPageSize()).orElse(20),
                Optional.ofNullable(mealSearch.getSortColumn()).orElse(MealSearch.SortColumn.dateTime),
                Optional.ofNullable(mealSearch.getSortDirection()).orElse(Sort.Direction.ASC)
        );
    }

    @PreAuthorize("@securityService.isLoggedUser(#userId) or hasRole('ADMIN')")
    public Meal getMeal(Long userId, Long mealId) {
        return handleUserMealOptional(
                findMealAndThen(userId, mealId, (user, meal) -> meal)
        );
    }

    @PreAuthorize("@securityService.isLoggedUser(#userId) or hasRole('ADMIN')")
    public Meal deleteMeal(Long userId, Long mealId) {
        return handleUserMealOptional(
                findMealAndThen(userId, mealId, (user, meal) -> {
                    mealRepository.delete(meal);
                    return meal;
                })
        );
    }

    @PreAuthorize("(@securityService.isLoggedUser(#userId)) or hasRole('ADMIN')")
    public Meal editMeal(Long userId, Long mealId, MealEdit mealEdit) {
        return handleUserMealOptional(
                findMealAndThen(userId, mealId, (user, meal) -> {
                    meal.setLocalDate(mealEdit.getLocalDate());
                    meal.setLocalTime(mealEdit.getLocalTime());
                    meal.setDescription(mealEdit.getDescription());
                    meal.setCalories(mealEdit.getCalories());
                    mealRepository.save(meal);
                    return meal;
                })
        );
    }

    @PreAuthorize("(@securityService.isLoggedUser(#userId)) or hasRole('ADMIN')")
    public Meal addMeal(Long userId, MealEdit mealEdit) {
        return userRepository.findById(userId)
                .map(user -> mealRepository.save(new Meal(
                                mealEdit.getLocalDate(),
                                mealEdit.getLocalTime(),
                                mealEdit.getDescription(),
                                mealEdit.getCalories(),
                                user
                        )
                ))
                .orElseGet(securityService::handleMissingUser);
    }

    private Optional<UserMeal> findMealAndThen(Long userId, Long mealId, BiFunction<User, Meal, Meal> callback) {
        return userRepository.findById(userId).map(user -> {
                    Optional<Meal> optionalMeal = mealRepository.findById(mealId).map(meal -> {
                        if (meal.getUser().getId() != user.getId()) {
                            throw new ResourceForbiddenException();
                        } else {
                            return callback.apply(user, meal);
                        }
                    });
                    return new UserMeal(user, optionalMeal);
                }
        );
    }

    /**
     * Extracts Meal from Optional<UserMeal>
     *
     * @return if user and meal are defined -> Meal
     *         if user is defined, but meal is not defined -> MealNotFoundException
     *         if user is not defined -> delegates to SecurityService#handleMissingUser()
     */
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private Meal handleUserMealOptional(Optional<UserMeal> userMealOptional) {
        return userMealOptional
                .map(userMeal -> userMeal.getMealOptional().orElseThrow(MealNotFoundException::new))
                .orElseGet(securityService::handleMissingUser);
    }


}
