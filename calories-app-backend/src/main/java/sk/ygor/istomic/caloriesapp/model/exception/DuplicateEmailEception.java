package sk.ygor.istomic.caloriesapp.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class DuplicateEmailEception extends RuntimeException {

    public DuplicateEmailEception() {
        super("Email already exists");
    }
}
