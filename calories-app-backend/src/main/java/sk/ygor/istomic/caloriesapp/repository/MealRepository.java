package sk.ygor.istomic.caloriesapp.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sk.ygor.istomic.caloriesapp.domain.Meal;

import java.time.LocalDate;

@Repository
public interface MealRepository extends PagingAndSortingRepository<Meal, Long>, JpaSpecificationExecutor<Meal> {

    @Query("select sum(meal.calories) from Meal meal where meal.user.id = :userId and meal.localDate = :localDate")
    int getSumOfCalories(@Param("userId") long userId,
                         @Param("localDate") LocalDate localDate);

}
