package sk.ygor.istomic.caloriesapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class User {

    public enum Role {
        USER,
        USER_MANAGER,
        ADMIN,
        ;

        public boolean isAdminOrUserManager() {
            return this == ADMIN || this == USER_MANAGER;
        }

        public boolean isAdmin() {
            return this == ADMIN;
        }

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @JsonIgnore
    @Column(nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Role role;

    @Column(nullable = false)
    private int dailyCaloriesLimit;

    protected User() {
    }

    public User(String email, String firstName, String lastName, String password, Role role, int dailyCaloriesLimit) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.role = role;
        this.dailyCaloriesLimit = dailyCaloriesLimit;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getDailyCaloriesLimit() {
        return dailyCaloriesLimit;
    }

    public void setDailyCaloriesLimit(int dailyCaloriesLimit) {
        this.dailyCaloriesLimit = dailyCaloriesLimit;
    }

    public User withId(Long id) {
        this.id = id;
        return this;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                dailyCaloriesLimit == user.dailyCaloriesLimit &&
                email.equals(user.email) &&
                firstName.equals(user.firstName) &&
                lastName.equals(user.lastName) &&
                password.equals(user.password) &&
                role == user.role;
    }

    @Override public int hashCode() {
        return Objects.hash(id, email, firstName, lastName, password, role, dailyCaloriesLimit);
    }

    @Override public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + "*****" + '\'' +
                ", role=" + role +
                ", dailyCaloriesLimit=" + dailyCaloriesLimit +
                '}';
    }

}
