package sk.ygor.istomic.caloriesapp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import sk.ygor.istomic.caloriesapp.controller.AccountController;
import sk.ygor.istomic.caloriesapp.controller.ApiController;

import javax.servlet.http.HttpServletResponse;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final JWTAuthorizationService jwtAuthorizationService;

    @Autowired
    public WebSecurityConfig(JWTAuthorizationService jwtAuthorizationService) {
        this.jwtAuthorizationService = jwtAuthorizationService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().disable(); // TODO: keep only in dev
        http.csrf().disable(); // TODO: keep only in dev

        http.addFilterAfter(jwtAuthorizationService.createFilter(), BasicAuthenticationFilter.class);
        http.authorizeRequests()
                .antMatchers(ApiController.PATH_API).permitAll()
                .antMatchers(AccountController.PATH_REGISTER).permitAll()
                .antMatchers(AccountController.PATH_LOGIN).permitAll()
                .antMatchers(AccountController.PATH_LOGOUT).permitAll()
                .antMatchers(AccountController.PATH_REFRESH_TOKEN).permitAll()
                .antMatchers("/api/**").authenticated()
                .anyRequest().permitAll()
        ;
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.exceptionHandling().authenticationEntryPoint((request, response, authException) -> {
            response.setHeader("WWW-Authenticate", "Bearer");
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
        });
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

/**
 @Bean CorsConfigurationSource corsConfigurationSource() {
 final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
 source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
 return source;
 }
 **/
}
