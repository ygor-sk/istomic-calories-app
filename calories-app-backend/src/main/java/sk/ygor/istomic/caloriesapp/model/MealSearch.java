package sk.ygor.istomic.caloriesapp.model;

import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDate;
import java.time.LocalTime;

public class MealSearch {

    public enum SortColumn {
        dateTime,
        description,
        calories,
        count,
    }

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dateFrom;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dateTo;

    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime timeFrom;
    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime timeTo;

    private String descriptionLike;

    @Digits(integer = 7, fraction = 0)
    @PositiveOrZero
    private Integer caloriesFrom;

    @Digits(integer = 7, fraction = 0)
    @PositiveOrZero
    private Integer caloriesTo;

    @PositiveOrZero
    private Integer pageNumber;

    @PositiveOrZero
    private Integer pageSize;

    private SortColumn sortColumn;
    private Sort.Direction sortDirection;

    public MealSearch() {
    }

    // TODO: group into smaller classes
    public MealSearch(String descriptionLike,
                      LocalDate dateFrom, LocalDate dateTo,
                      LocalTime timeFrom, LocalTime timeTo,
                      Integer caloriesFrom, Integer caloriesTo,
                      Integer pageNumber, Integer pageSize,
                      SortColumn sortColumn, Sort.Direction sortDirection) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.descriptionLike = descriptionLike;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.caloriesFrom = caloriesFrom;
        this.caloriesTo = caloriesTo;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.sortColumn = sortColumn;
        this.sortDirection = sortDirection;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public String getDescriptionLike() {
        return descriptionLike;
    }

    public Integer getCaloriesFrom() {
        return caloriesFrom;
    }

    public Integer getCaloriesTo() {
        return caloriesTo;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public SortColumn getSortColumn() {
        return sortColumn;
    }

    public Sort.Direction getSortDirection() {
        return sortDirection;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public void setDescriptionLike(String descriptionLike) {
        this.descriptionLike = descriptionLike;
    }

    public void setCaloriesFrom(Integer caloriesFrom) {
        this.caloriesFrom = caloriesFrom;
    }

    public void setCaloriesTo(Integer caloriesTo) {
        this.caloriesTo = caloriesTo;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public void setSortColumn(SortColumn sortColumn) {
        this.sortColumn = sortColumn;
    }

    public void setSortDirection(Sort.Direction sortDirection) {
        this.sortDirection = sortDirection;
    }

    public LocalTime getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(LocalTime timeFrom) {
        this.timeFrom = timeFrom;
    }

    public LocalTime getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(LocalTime timeTo) {
        this.timeTo = timeTo;
    }
}
