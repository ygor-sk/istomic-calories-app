package sk.ygor.istomic.caloriesapp.security;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import sk.ygor.istomic.caloriesapp.domain.User;
import sk.ygor.istomic.caloriesapp.model.exception.ResourceForbiddenException;
import sk.ygor.istomic.caloriesapp.model.exception.UserNotFoundException;

@Service
public class SecurityService {

    public boolean isLoggedUser(Long userId) {
        return getLoggedUserId().equals(userId);
    }

    public Long getLoggedUserId() {
        return getAuthentication().getUserId();
    }

    public User.Role getLoggedRole() {
        return getAuthentication().getRole();
    }

    public <T> T handleMissingUser() {
        if (getLoggedRole().isAdminOrUserManager()) {
            throw new UserNotFoundException();
        } else {
            throw new ResourceForbiddenException(); // don't allow to probe user IDs
        }
    }

    private JWTAuthentication getAuthentication() {
        return (JWTAuthentication) SecurityContextHolder.getContext().getAuthentication();
    }

}
