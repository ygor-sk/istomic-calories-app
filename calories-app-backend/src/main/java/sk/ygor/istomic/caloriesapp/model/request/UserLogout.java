package sk.ygor.istomic.caloriesapp.model.request;

import javax.validation.constraints.NotBlank;

public class UserLogout {

    private final long userId;
    private final String refreshToken;

    public UserLogout(long userId, @NotBlank String refreshToken) {
        this.userId = userId;
        this.refreshToken = refreshToken;
    }

    public long getUserId() {
        return userId;
    }

    public String getRefreshToken() {
        return refreshToken;
    }
}
