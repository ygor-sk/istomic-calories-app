package sk.ygor.istomic.caloriesapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sk.ygor.istomic.caloriesapp.domain.User;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByEmail(@Param("email") String email);

    // TODO: optimistic locking
    /*
    server -> client return version in E-Tag
    client -> server send back version in If-Match

    if If-Match value doesn't match the data store’s version value, throw HTTP 412 Precondition Failed

     */
}
