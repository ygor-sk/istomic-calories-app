package sk.ygor.istomic.caloriesapp.domain;

import java.time.LocalDate;
import java.util.Objects;

public class MealGrouped {

    private final LocalDate localDate;
    private final long count;
    private final long totalCalories;

    public MealGrouped(LocalDate localDate, long count, long totalCalories) {
        this.localDate = localDate;
        this.count = count;
        this.totalCalories = totalCalories;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public long getCount() {
        return count;
    }

    public long getTotalCalories() {
        return totalCalories;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MealGrouped that = (MealGrouped) o;
        return count == that.count &&
                totalCalories == that.totalCalories &&
                localDate.equals(that.localDate);
    }

    @Override public int hashCode() {
        return Objects.hash(localDate, count, totalCalories);
    }

    @Override public String toString() {
        return "MealGrouped{" +
                "localDate=" + localDate +
                ", count=" + count +
                ", totalCalories=" + totalCalories +
                '}';
    }
}
