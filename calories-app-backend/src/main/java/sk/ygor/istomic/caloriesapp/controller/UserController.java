package sk.ygor.istomic.caloriesapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import sk.ygor.istomic.caloriesapp.domain.User;
import sk.ygor.istomic.caloriesapp.model.request.ResetPassword;
import sk.ygor.istomic.caloriesapp.model.request.UserEdit;
import sk.ygor.istomic.caloriesapp.services.UserService;

import javax.validation.Valid;

@RestController
public class UserController {

    private final UserService userService;

    public static final String PATH_GET_ALL = "/api/user";
    public static final String PATH_GET_SINGLE = "/api/user/{userId}";
    public static final String PATH_POST_SINGLE_EDIT = "/api/user/{userId}/edit";
    public static final String PATH_POST_SINGLE_RESET_PASSWORD = "/api/user/{userId}/resetPassword";

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(path = PATH_GET_ALL, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @Transactional(readOnly = true)
    public @ResponseBody Iterable<User> allUsers() {
        return userService.findAllUsers();
    }

    @RequestMapping(path = PATH_GET_SINGLE, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @Transactional(readOnly = true)
    public @ResponseBody User getUser(@PathVariable Long userId) {
        return userService.findUserById(userId);
    }

    @RequestMapping(path = PATH_POST_SINGLE_EDIT, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    public @ResponseBody User userPostSingleEdit(@PathVariable Long userId, @Valid @RequestBody UserEdit userEdit) {
        return userService.editUser(userId, userEdit);
    }

    @RequestMapping(path = PATH_POST_SINGLE_RESET_PASSWORD, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    public @ResponseBody ResetPassword userPostSingleResetPassword(@PathVariable Long userId) {
        return userService.resetPassword(userId);
    }

}
