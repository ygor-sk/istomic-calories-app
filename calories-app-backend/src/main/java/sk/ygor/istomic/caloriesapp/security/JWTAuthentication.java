package sk.ygor.istomic.caloriesapp.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import sk.ygor.istomic.caloriesapp.domain.User;

import java.util.Collection;
import java.util.Collections;

/**
 * Represents all information, which has been extracted from JWT.
 */
public class JWTAuthentication implements Authentication {

    private static final String ROLE_PREFIX = "ROLE_";

    private final long userId;
    private final User.Role role;

    public JWTAuthentication(long userId, User.Role role) {
        this.userId = userId;
        this.role = role;
    }

    public long getUserId() {
        return userId;
    }

    public User.Role getRole() {
        return this.role;
    }

    @Override public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(ROLE_PREFIX + role.name()));
    }

    @Override public Object getCredentials() {
        return null;
    }

    @Override public Object getDetails() {
        return null;
    }

    @Override public Long getPrincipal() {
        return userId;
    }

    @Override public boolean isAuthenticated() {
        return true;
    }

    @Override public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        throw new UnsupportedOperationException("JWT authentication is always authenticated");
    }

    @Override public String getName() {
        return "user_" + userId;
    }
}
