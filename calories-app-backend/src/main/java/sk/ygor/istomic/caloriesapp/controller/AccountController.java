package sk.ygor.istomic.caloriesapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import sk.ygor.istomic.caloriesapp.domain.User;
import sk.ygor.istomic.caloriesapp.model.request.UserLogin;
import sk.ygor.istomic.caloriesapp.model.request.UserLogout;
import sk.ygor.istomic.caloriesapp.model.request.UserRefresh;
import sk.ygor.istomic.caloriesapp.model.request.UserRegistration;
import sk.ygor.istomic.caloriesapp.model.response.AccessToken;
import sk.ygor.istomic.caloriesapp.model.response.UserLoginResponse;
import sk.ygor.istomic.caloriesapp.security.SecurityService;
import sk.ygor.istomic.caloriesapp.services.AccountService;
import sk.ygor.istomic.caloriesapp.services.UserService;

import javax.validation.Valid;

@RestController
public class AccountController {

    public static final String PATH_REGISTER = "/api/account/register";
    public static final String PATH_LOGIN = "/api/account/login";
    public static final String PATH_LOGOUT = "/api/account/logout";
    public static final String PATH_REFRESH_TOKEN = "/api/account/refreshToken";
    public static final String PATH_GET_LOGGED = "/api/account/logged";

    private final AccountService accountService;
    private final UserService userService;
    private final SecurityService securityService;

    @Autowired
    public AccountController(AccountService accountService,
                             UserService userService,
                             SecurityService securityService) {
        this.accountService = accountService;
        this.userService = userService;
        this.securityService = securityService;
    }

    @RequestMapping(path = PATH_REGISTER, method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    User accountPostRegister(@Valid @RequestBody UserRegistration userRegistration) {
        return accountService.registerUser(userRegistration);
    }

    @RequestMapping(path = PATH_LOGIN, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @ResponseBody
    UserLoginResponse accountPostLogin(@Valid @RequestBody UserLogin userLogin) {
        return accountService.loginUser(userLogin);
    }

    @RequestMapping(path = PATH_LOGOUT, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    void accountPostLogout(@Valid @RequestBody UserLogout userLogout) {
        accountService.logoutUser(userLogout.getUserId(), userLogout.getRefreshToken());
    }

    @RequestMapping(path = PATH_REFRESH_TOKEN, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    AccessToken accountPostRefreshToken(@Valid @RequestBody UserRefresh userRefresh) {
        return accountService.refreshAccessToken(userRefresh.getUserId(), userRefresh.getRefreshToken());
    }

    @RequestMapping(path = PATH_GET_LOGGED, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    User accountGetLogged() {
        Long loggedUserId = securityService.getLoggedUserId();
        return userService.findUserById(loggedUserId);
    }

}
