package sk.ygor.istomic.caloriesapp.mvc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriTemplate;
import sk.ygor.istomic.caloriesapp.MockData;
import sk.ygor.istomic.caloriesapp.controller.UserController;
import sk.ygor.istomic.caloriesapp.domain.User;

import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerMvcTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserController userController;

    @Test
    @WithMockUser
    public void allUsers() throws Exception {
        given(userController.allUsers()).willReturn(Arrays.asList(
                MockData.createTestUserWithId(10),
                MockData.createTestUserWithId(11),
                MockData.createTestUserWithId(12)
        ));

        User testUser = MockData.createTestUserNoId();

        mvc.perform(get(new UriTemplate(UserController.PATH_GET_ALL).expand())
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.length()", is(3)))
                .andExpect(jsonPath("$[0].id", is(10)))
                .andExpect(jsonPath("$[1].id", is(11)))
                .andExpect(jsonPath("$[2].id", is(12)))
                .andExpect(jsonPath("$[2].lastName", is(testUser.getLastName())))
                .andExpect(jsonPath("$[2].dailyCaloriesLimit", is(testUser.getDailyCaloriesLimit())))
                .andExpect(jsonPath("$[2].email", is(testUser.getEmail())))
                .andExpect(jsonPath("$[2].password").doesNotExist()) // IMPORTANT !!!
                .andExpect(jsonPath("$[2].role", is(testUser.getRole().name())))
                .andExpect(jsonPath("$[2].firstName", is(testUser.getFirstName())))
        ;
    }

    @Test
    @WithMockUser
    public void getUser() throws Exception {
        User testUser = MockData.createTestUserWithId(56);
        given(userController.getUser((long) 56)).willReturn(testUser);

        mvc.perform(get(new UriTemplate(UserController.PATH_GET_SINGLE).expand(testUser.getId()))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is((int) testUser.getId())))
                .andExpect(jsonPath("$.email", is(testUser.getEmail())))
        ;
    }

}