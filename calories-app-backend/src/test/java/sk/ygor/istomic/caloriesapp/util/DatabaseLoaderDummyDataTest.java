package sk.ygor.istomic.caloriesapp.util;

import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;
import sk.ygor.istomic.caloriesapp.BaseIntegrationTest;
import sk.ygor.istomic.caloriesapp.CaloriesApp;

@ActiveProfiles(CaloriesApp.PROFILE_LOAD_DUMMY_DATA)
public class DatabaseLoaderDummyDataTest extends BaseIntegrationTest {

    @Test
    public void testLoadDummyData() {
        // just testing, that the profile works
        // this is only to put coverage on the DatabaseLoader
    }
}
