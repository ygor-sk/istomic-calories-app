package sk.ygor.istomic.caloriesapp;

import sk.ygor.istomic.caloriesapp.domain.Meal;
import sk.ygor.istomic.caloriesapp.domain.User;

import java.time.LocalDateTime;

public class MockData {

    public static User createTestUserWithId(long id) {
        User user = createTestUserNoId();
        user.setId(id);
        return user;
    }

    public static User createTestUserNoId() {
        return new User("test1", "Test1", "Test1", "h4f894hnf", User.Role.USER, 1520);
    }


    public static Meal createMeal(LocalDateTime dateTime, String description, int calories, User user) {
        return new Meal(dateTime.toLocalDate(), dateTime.toLocalTime(), description, calories, user);
    }
}
