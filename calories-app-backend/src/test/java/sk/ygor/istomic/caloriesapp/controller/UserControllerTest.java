package sk.ygor.istomic.caloriesapp.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import sk.ygor.istomic.caloriesapp.MockData;
import sk.ygor.istomic.caloriesapp.domain.User;
import sk.ygor.istomic.caloriesapp.services.UserService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class UserControllerTest {

    @MockBean
    private UserService userService;

    private UserController userController;

    @Before
    public void setUp() {
        userController = new UserController(userService);
    }

    @Test
    public void getUser_exists() {
        given(userService.findUserById(any())).willReturn(MockData.createTestUserWithId(512L));

        User user = userController.getUser(42L);
        assertThat(user).isNotNull();
        assertThat(user.getId()).isEqualTo(512L);
    }

    @Test
    public void getUser_not_exists() {
        given(userService.findUserById(any())).willThrow(new RuntimeException("user not found"));

        assertThatThrownBy(() -> userController.getUser(42L))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("user not found");
    }

}