package sk.ygor.istomic.caloriesapp.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import sk.ygor.istomic.caloriesapp.MockData;
import sk.ygor.istomic.caloriesapp.domain.User;

import java.util.Optional;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenFindByEmail_thenReturnUser() {
        // given
        User user = entityManager.persist(MockData.createTestUserNoId());
        entityManager.flush();

        // when
        Optional<User> foundOptional = userRepository.findByEmail(user.getEmail());
        assertThat(foundOptional).get().isEqualTo(user);
    }

}