package sk.ygor.istomic.caloriesapp.services;

import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import sk.ygor.istomic.caloriesapp.BaseIntegrationTest;
import sk.ygor.istomic.caloriesapp.domain.Meal;
import sk.ygor.istomic.caloriesapp.domain.MealGrouped;
import sk.ygor.istomic.caloriesapp.domain.User;
import sk.ygor.istomic.caloriesapp.model.MealSearch;
import sk.ygor.istomic.caloriesapp.model.response.MealSearchResult;
import sk.ygor.istomic.caloriesapp.security.JWTAuthentication;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static sk.ygor.istomic.caloriesapp.MockData.createMeal;

/**
 * We don't want to test how Specification and Pageable are constructed.
 * We will rather check what results they provide.
 * <p>
 * For that reason we use an integration test instead of a simple unit test.
 */
public class MealServiceTest extends BaseIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MealService mealService;

    private User testUser;
    private Meal mealTwo;
    private Meal mealOne;

    @Before
    public void setUp() {
        testUser = entityManager.persist(
                new User("search", "search", "search", "search", User.Role.USER, 3000)
        );
        entityManager.persist(createMeal(LocalDateTime.parse("2018-10-03T10:00:00"), "Chicken breast with rice 1200", 1200, testUser));
        entityManager.persist(createMeal(LocalDateTime.parse("2018-10-04T10:00:00"), "Chicken breast with rice 2100", 1200, testUser));
        entityManager.persist(createMeal(LocalDateTime.parse("2018-10-05T10:00:00"), "Chicken breast with rice 2200", 1200, testUser));
        // expected total result
        entityManager.persist(createMeal(LocalDateTime.parse("2018-10-06T10:00:00"), "Chicken breast with rice 2300", 2300, testUser));
        entityManager.persist(createMeal(LocalDateTime.parse("2018-10-07T10:00:00"), "Chicken breast with rice 2400", 2400, testUser));
        // // expected page result
        mealOne = entityManager.persist(createMeal(LocalDateTime.parse("2018-10-08T10:00:00"), "Chicken breast with rice 2500", 2500, testUser));
        mealTwo = entityManager.persist(createMeal(LocalDateTime.parse("2018-10-09T10:00:00"), "Chicken breast with rice 2600", 2600, testUser));
        // // expected page result
        entityManager.persist(createMeal(LocalDateTime.parse("2018-10-09T10:10:00"), "Chicken breast with rice 2700", 2700, testUser));
        entityManager.persist(createMeal(LocalDateTime.parse("2018-10-10T10:00:00"), "Chicken breast with rice 2800", 2800, testUser));
        // expected total result
        entityManager.persist(createMeal(LocalDateTime.parse("2018-10-11T10:00:00"), "Chicken breast with rice 2800", 2850, testUser));
        entityManager.persist(createMeal(LocalDateTime.parse("2018-10-12T10:10:00"), "Chicken breast with rice 2900", 2900, testUser));
        entityManager.persist(createMeal(LocalDateTime.parse("2018-10-12T10:20:00"), "Chicken Kebab", 2500, testUser));
        entityManager.persist(createMeal(LocalDateTime.parse("2018-10-13T10:00:00"), "Veal Kebab", 2550, testUser));
        entityManager.persist(createMeal(LocalDateTime.parse("2018-10-13T10:10:00"), "Water", 0, testUser));
        entityManager.flush();

        SecurityContextHolder.getContext().setAuthentication(new JWTAuthentication(testUser.getId(), User.Role.USER));
    }

    @After
    public void tearDown() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void test_searchMeals() {
        MealSearch mealSearch = new MealSearch(
                "rice",
                LocalDate.parse("2018-10-06"),
                LocalDate.parse("2018-10-10"),
                null, null,
                2150, 2850,
                1, 2,
                MealSearch.SortColumn.calories, Sort.Direction.DESC
        );

        MealSearchResult<Long, Meal> result = mealService.searchMeals(testUser.getId(), mealSearch);

        assertThat(result.getPage().getTotalPages()).isEqualTo(3);
        assertThat(result.getPage().getContent().size()).isEqualTo(2);
        assertThat(result.getPage().getContent().get(0).getDescription()).isEqualTo("Chicken breast with rice 2600");
        assertThat(result.getPage().getContent().get(1).getDescription()).isEqualTo("Chicken breast with rice 2500");
        assertThat(result.getTotalCalories()).isEqualTo(2300 + 2400 + 2500 + 2600 + 2700 + 2800);
        assertThat(result.getCaloriesStatus()).isEqualTo(ImmutableMap.builder()
                .put(mealOne.getId(), MealSearchResult.CaloriesStatus.BELOW_LIMIT)
                .put(mealTwo.getId(), MealSearchResult.CaloriesStatus.EXCEEDED)
                .build()
        );
    }

    @Test
    public void test_searchMealsGrouped() {
        MealSearch mealSearch = new MealSearch(
                "rice",
                LocalDate.parse("2018-10-06"),
                LocalDate.parse("2018-10-10"),
                null, null,
                2150, 2850,
                1, 3,
                MealSearch.SortColumn.calories, Sort.Direction.ASC
        );

        MealSearchResult<LocalDate, MealGrouped> result = mealService.searchMealsGrouped(testUser.getId(), mealSearch);

        assertThat(result.getPage().getTotalPages()).isEqualTo(2);
        assertThat(result.getPage().getContent().size()).isEqualTo(2);
        assertThat(result.getPage().getContent().get(0)).isEqualTo(new MealGrouped(LocalDate.parse("2018-10-10"), 1, 2800));
        assertThat(result.getPage().getContent().get(1)).isEqualTo(new MealGrouped(LocalDate.parse("2018-10-09"), 2, 2600 + 2700));
    }

}
