package sk.ygor.istomic.caloriesapp.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import sk.ygor.istomic.caloriesapp.MockData;
import sk.ygor.istomic.caloriesapp.domain.User;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static sk.ygor.istomic.caloriesapp.MockData.createMeal;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MealRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MealRepository mealRepository;

    @Test
    public void testSumOfCalories() {
        User testUser = entityManager.persist(MockData.createTestUserNoId());

        // given
        entityManager.persist(createMeal(LocalDateTime.parse("2015-10-04T14:20:25"), "Meal A", 1000, testUser));
        entityManager.persist(createMeal(LocalDateTime.parse("2015-10-05T15:20:25"), "Meal B", 2100, testUser));
        entityManager.persist(createMeal(LocalDateTime.parse("2015-10-05T16:20:25"), "Meal C", 3230, testUser));
        entityManager.flush();

        int dailyCaloriesSum = mealRepository.getSumOfCalories(
                testUser.getId(),
                LocalDate.parse("2015-10-05")

        );
        assertThat(dailyCaloriesSum).isEqualByComparingTo(2100 + 3230);
    }

}