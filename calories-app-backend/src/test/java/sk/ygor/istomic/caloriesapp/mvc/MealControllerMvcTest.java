package sk.ygor.istomic.caloriesapp.mvc;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriTemplate;
import sk.ygor.istomic.caloriesapp.MockData;
import sk.ygor.istomic.caloriesapp.controller.MealController;
import sk.ygor.istomic.caloriesapp.model.response.MealSearchResult;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static sk.ygor.istomic.caloriesapp.MockData.createMeal;

@RunWith(SpringRunner.class)
@WebMvcTest(MealController.class)
public class MealControllerMvcTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MealController mealController;

    @Test
    @WithMockUser
    public void mealsGetSearch() throws Exception {
        given(mealController.mealGetSearch(eq(44L), any())).willReturn(new MealSearchResult<>(
                1678,
                new PageImpl<>(
                        Arrays.asList(
                                createMeal(LocalDateTime.parse("2016-07-08T13:33:44"), "Meal One", 1455, MockData.createTestUserNoId()).withId(653L),
                                createMeal(LocalDateTime.parse("2016-07-08T13:33:45"), "Meal Two", 672, MockData.createTestUserNoId()).withId(654L)
                        ),
                        PageRequest.of(10, 11),
                        17
                ),
                ImmutableMap.<Long, MealSearchResult.CaloriesStatus>builder()
                        .put(15L, MealSearchResult.CaloriesStatus.EXCEEDED)
                        .put(16L, MealSearchResult.CaloriesStatus.BELOW_LIMIT)
                        .build()
        ));

        mvc.perform(get(new UriTemplate(MealController.PATH_GET_SEARCH).expand(44L))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.totalCalories", is(1678)))
                .andExpect(jsonPath("$.caloriesStatus.length()", is(2)))
                .andExpect(jsonPath("$.caloriesStatus.15", is(MealSearchResult.CaloriesStatus.EXCEEDED.name())))
                .andExpect(jsonPath("$.caloriesStatus.16", is(MealSearchResult.CaloriesStatus.BELOW_LIMIT.name())))
                .andExpect(jsonPath("$.page.content").isArray())
                .andExpect(jsonPath("$.page.content.length()", is(2)))
                .andExpect(jsonPath("$.page.content[0].id", is(653)))
                .andExpect(jsonPath("$.page.content[0].description", is("Meal One")))
                .andExpect(jsonPath("$.page.content[0].calories", is(1455)))
                .andExpect(jsonPath("$.page.content[0].localDate", is("2016-07-08")))
                .andExpect(jsonPath("$.page.content[0].localTime", is("13:33:44")))
                .andExpect(jsonPath("$.page.content[1].calories", is(672)))
        ;
    }

}