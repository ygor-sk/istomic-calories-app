package sk.ygor.istomic.caloriesapp.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import sk.ygor.istomic.caloriesapp.model.MealSearch;
import sk.ygor.istomic.caloriesapp.model.response.MealSearchResult;
import sk.ygor.istomic.caloriesapp.security.SecurityService;
import sk.ygor.istomic.caloriesapp.services.MealService;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.*;

@RunWith(SpringRunner.class)
public class MealControllerTest {

    @MockBean
    private MealService mealService;

    private MealController mealController;

    @Before
    public void setUp() {
        mealController = new MealController(mealService);
    }

    @Test
    public void searchMeal_user_exists() {
        given(mealService.searchMeals(eq(42L), any()))
                .willReturn(new MealSearchResult<>(123, null, null));

        MealSearchResult searchResult = mealController.mealGetSearch(42L, new MealSearch());
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getTotalCalories()).isEqualTo(123);
    }

    @Test
    public void searchMeal_user_not_exists() {
        given(mealService.searchMeals(eq(42L), any())).willThrow(new RuntimeException("missing user"));
        assertThatThrownBy(() -> mealController.mealGetSearch(42L, new MealSearch()))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("missing user");
    }
}