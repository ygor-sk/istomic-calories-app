package sk.ygor.istomic.caloriesapp.services;

import org.assertj.core.util.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import sk.ygor.istomic.caloriesapp.BaseIntegrationTest;
import sk.ygor.istomic.caloriesapp.domain.User;
import sk.ygor.istomic.caloriesapp.model.request.ResetPassword;
import sk.ygor.istomic.caloriesapp.security.JWTAuthentication;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * We don't want to test how Specification and Pageable are constructed.
 * We will rather check what results they provide.
 * <p>
 * For that reason we use an integration test instead of a simple unit test.
 */
public class UserServiceTest extends BaseIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private User testUserOne;
    private User testUserTwo;
    private User testUserThree;

    @Before
    public void setUp() {
        testUserOne = entityManager.persist(new User("userOne", "search", "search", "password", User.Role.USER, 3000));
        testUserTwo = entityManager.persist(new User("userTwo", "search", "search", "password", User.Role.USER, 3000));
        testUserThree = entityManager.persist(new User("userThree", "search", "search", "password", User.Role.USER, 3000));
        entityManager.flush();

    }

    @After
    public void tearDown() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void test_searchUsers_admin() {
        SecurityContextHolder.getContext().setAuthentication(new JWTAuthentication(42L, User.Role.ADMIN));

        List<User> users = Lists.newArrayList(userService.findAllUsers());
        assertThat(users.size()).isEqualTo(3);
        assertThat(users.get(0).getId()).isEqualTo(testUserOne.getId());
        assertThat(users.get(1).getId()).isEqualTo(testUserTwo.getId());
        assertThat(users.get(2).getId()).isEqualTo(testUserThree.getId());
        assertThat(users.get(2).getEmail()).isEqualTo(testUserThree.getEmail());
    }

    @Test
    public void test_searchUsers_user() {
        SecurityContextHolder.getContext().setAuthentication(new JWTAuthentication(42L, User.Role.USER));
        assertThatThrownBy(() -> userService.findAllUsers()).isInstanceOf(AccessDeniedException.class);
    }

    @Test
    public void test_resetPassword() {
        SecurityContextHolder.getContext().setAuthentication(new JWTAuthentication(testUserTwo.getId(), User.Role.USER));
        ResetPassword resetPassword = userService.resetPassword(testUserTwo.getId());
        assertThat(resetPassword.getNewPassword()).isNotEqualTo("password");

        assertThat(passwordEncoder.matches(
                resetPassword.getNewPassword(),
                entityManager.find(User.class, testUserTwo.getId()).getPassword()
        )).isTrue();
    }

}
